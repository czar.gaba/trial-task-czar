<?php include('functions.php');?>
<?php include($partials.'header.php');?>
<?php 
// uncomment for session auto start
// session_starter();
?>
<body class="<?php fileclass();?>">
 
<?php 
require_once 'vendor/autoload.php';
 
// init configuration
$clientID = '692063515282-rdukn4cvjig8tukhctfalqplakdmt9le.apps.googleusercontent.com';
$clientSecret = 'GosT3EBI3vLqgNVWMAGlGOXu';
$redirectUri = 'http://localhost/trial-task-czar/google.php';
  
// create Client Request to access Google API
$client = new Google_Client();
$client->setClientId($clientID);
$client->setClientSecret($clientSecret);
$client->setRedirectUri($redirectUri);
$client->addScope("email");
$client->addScope("profile");
?>
 
<div class="container">
    <div class="row text-center">
        <div class="col-md-4"><a href="facebook.php" class="btn btn-info">login with facebook</a></div>
        <div class="col-md-4"><a href="linkedin.php" class="btn btn-primary">login with linkedin</a></div>
        <div class="col-md-4">
        <?php 
        // authenticate code from Google OAuth Flow
        if (isset($_GET['code'])) {
            $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
            $client->setAccessToken($token['access_token']);
            
            // get profile info
            $google_oauth = new Google_Service_Oauth2($client);
            $google_account_info = $google_oauth->userinfo->get();
            $email =  $google_account_info->email;
            $name =  $google_account_info->name;
        
            // now you can use this profile info to create account in your website and make user logged in.
        } else {
            echo "<a class='btn btn-warning' href='".$client->createAuthUrl()."'>Google Login</a>";
        }?>

       
        </div>
    </div>
</div>


 <?php include($partials.'footer.php');?>