


<?php include('functions.php');?>
<?php include($partials.'header.php');?>
<?php 
// uncomment for session auto start
 session_starter();
?>
<body class="<?php fileclass();?>">
 

 <div class="container">
    <div class="row">
        <div class="col-md-12">
        <!-- start -->
        <?php
            // Include the autoloader provided in the SDK
            require_once __DIR__ . '/vendor/autoload.php';
        
            // Include required libraries
            use Facebook\Facebook;
            use Facebook\Exceptions\FacebookResponseException;
            use Facebook\Exceptions\FacebookSDKException;
            

            
            $fb = new Facebook(array(
                'app_id' => $appId,
                'app_secret' => $appSecret,
                'default_graph_version' => 'v5.0',
                    ));
            
            // Get redirect login helper
            $helper = $fb->getRedirectLoginHelper();
            
            // Try to get access token
            try {
                // Already login
                if (isset($_SESSION['facebook_access_token'])) {
                    $accessToken = $_SESSION['facebook_access_token'];
                } else {
                    $accessToken = $helper->getAccessToken();
                }
            
                if (isset($accessToken)) {
                    if (isset($_SESSION['facebook_access_token'])) {
                        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
                    } else {
                        // Put short-lived access token in session
                        $_SESSION['facebook_access_token'] = (string) $accessToken;
            
                        // OAuth 2.0 client handler helps to manage access tokens
                        $oAuth2Client = $fb->getOAuth2Client();
            
                        // Exchanges a short-lived access token for a long-lived one
                        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
                        $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
            
                        // Set default access token to be used in script
                        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
                    }
            
                    // Redirect the user back to the same page if url has "code" parameter in query string
                    if (isset($_GET['code'])) {
            
                        // Getting user facebook profile info
                        try {
                            $profileRequest = $fb->get('/me?fields=name,first_name,last_name,email,link,gender,locale,picture');
                            $fbUserProfile = $profileRequest->getGraphNode()->asArray();
                            // Here you can redirect to your Home Page.
                            echo "<pre/>";
                            print_r($fbUserProfile);
                        } catch (FacebookResponseException $e) {
                            echo 'Graph returned an error: ' . $e->getMessage();
                            session_destroy();
                            // Redirect user back to app login page
                            header("Location: ./");
                            exit;
                        } catch (FacebookSDKException $e) {
                            echo 'Facebook SDK returned an error: ' . $e->getMessage();
                            exit;
                        }
                    }
                } else {
                    // Get login url
            
                    $loginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);
                    header("Location: " . $loginURL);
                    
                }
            } catch (FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            ?>

        <!-- end  -->
        </div>
    </div>
 </div>




<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php echo $fbUserProfile['email'];?>
            <a href="logout.php" class="btn btn-danger">logout</a>
        </div>
    </div>
</div>

<?php
// mail function here 
$to =  $fbUserProfile['email']; //
$subject = "Facebook Login";
$txt = "you are login in facebook";
$headers = "From: from@from.com" ;

mail($to,$subject,$txt,$headers);

?>

 <?php include($partials.'footer.php');?>