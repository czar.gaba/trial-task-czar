<?php
// include the nesesarry files
// include('includes/conn.php');
// include('includes/functions.php');

// usage

// GET usage
// $data = get('data');
// foreach ($data as $row) {
//     echo $row['name']."<br />\n";
// }


// GET where id 
// $data = get_whereid('data' , '1');
// foreach ($data as $row) {
//     echo $row['id']."<br />\n";
// }
// get where field
// $data = get_where_fieldvalue('data','name','test');
// foreach ($data as $row) {
//     echo $row['name']."<br />\n";
// }

// insert function usage
// $array = array(
// 	'name'=>'value'
// );
// if(insert($array,'data')){
// 	echo 'inserted';
// }else{
// 	echo 'error on insert';
// }

// update function usage
// $array = array(
// 	'name'=> 'value2'
// );
// if(update($array,'3','data')){
// 	echo 'updated';
// }else{
// 	echo 'error on update';
// }

// delete function usage
// if(delete('3','data')){
// 	echo 'deleted';
// }else{
// 	echo 'echo on query';
// }

// custom query usage
// $data = custom_query('select * from test');
// foreach ($data as $row) {
//     echo $row['id']."<br />\n";
// }

// count function usage
// $count = count_rows('data');
// echo $count;

// excerpt function usage
// $text = "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsa delectus exercitationem dolore, inventore at, autem molestiae dolorem, possimus ullam reiciendis suscipit rerum? Error quam rerum esse minus minima enim molestias.";
// $text = display_excerpt($text , 10);
// echo $text;