<?php
session_start();
session_destroy();

if ($adapter->isConnected()) {
    $adapter->disconnect();
   
}
header('location: ./');

